defmodule Strategy do

  @heirarchy_map %{
    "A" => "C", # rock beats scissors
    "B" => "A", # paper beats rock
    "C" => "B", # scissors beats paper
  }

  @normalize_map %{
    "X" => "A",
    "Y" => "B",
    "Z" => "C",
  }

  @player_score_map %{
    "A" => 1,
    "B" => 2,
    "C" => 3,
  }

  def score(filename) do
    filename |> File.stream!() |> Enum.map(&String.trim/1) |> Enum.map(&process_turn/1) |> Enum.sum()
  end

  defp process_turn(strategy) do
    elements = strategy |> String.split(" ")
    opponent = List.first(elements)
    normalized_player = Map.get(@normalize_map, List.last(elements))
    player_score = Map.get(@player_score_map, normalized_player)
    player_beats = Map.get(@heirarchy_map, normalized_player)

    case opponent do
      ^normalized_player -> 3 + player_score
      ^player_beats -> 6 + player_score
      _ -> player_score
    end

  end
end

[filename] = System.argv()
filename |> Strategy.score() |> IO.inspect()
