defmodule Calories do
  def find_mule(filename) do
    File.read!(filename)
      |> String.split("\n\n", trim: true)
      |> Enum.map(&process_elf/1)
      |> Enum.sort()
      |> Enum.reverse()
      |> Enum.take(3)
      |> Enum.sum()
  end

  defp process_elf(food_string) do
    food_string |> String.split("\n", trim: true) |> Enum.map(&String.to_integer/1) |> Enum.sum()
  end
end

[file] = System.argv()
Calories.find_mule(file) |> IO.inspect()
