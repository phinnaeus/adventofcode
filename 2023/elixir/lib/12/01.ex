defmodule HotSprings do
  @moduledoc """

  https://adventofcode.com/2023/day/12
  """

  def run(filename \\ "inputs/12/input.txt") do
    filename
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&parse_line/1)

    0
  end

  defp parse_line(line) do
    # After the list of springs for a given row, the size of each contiguous group of damaged springs
    # is listed in the order those groups appear in the row. This list always accounts for every damaged spring,
    # and each number is the entire size of its contiguous group
    # (that is, groups are always separated by at least one operational spring:
    # #### would always be 4, never 2,2).
    [gears, groups] = String.split(line, " ", parts: 2, trim: true)

    gears =
      gears
      |> String.to_charlist()
      |> Enum.with_index(fn e, i -> {i, convert_ascii(e)} end)

    {gears, parse_groups(groups)}
  end

  defp parse_groups(groups_text) do
    groups_text
    |> String.split(",", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  defp convert_ascii(a) do
    case a do
      46 -> :working
      35 -> :damaged
      63 -> :unknown
    end
  end
end
