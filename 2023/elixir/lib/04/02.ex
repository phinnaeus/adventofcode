defmodule Scratchcards2 do
  @moduledoc """
  https://adventofcode.com/2023/day/4
  """

  @card_pattern ~r/^Card\s+(\d+): (.*)$/

  def total_cards(filename \\ "inputs/04/input.txt") do
    filename
    |> File.stream!()
    |> Enum.map(&String.trim/1)
    |> Enum.map(&parse_line/1)
    |> Enum.map(&count_wins/1)
    |> count_cards()
  end

  defp count_cards(games) do
    games
    |> Enum.reduce(Map.new(games, fn {g, _} -> {g, 1} end), fn
      {_, 0}, acc ->
        acc

      {game, val}, res ->
        copies = get_copy_indices(game, val)
        current_card_count = Map.get(res, game)

        res
        |> Enum.map(fn {g, cnt} ->
          if g in copies do
            {g, cnt + current_card_count}
          else
            {g, cnt}
          end
        end)
        |> Map.new()
    end)
    |> Map.values()
    |> Enum.sum()
  end

  defp get_copy_indices(_, 0), do: []

  defp get_copy_indices(game, wins) do
    Enum.to_list((game + 1)..(game + wins))
  end

  defp count_wins({id, winning, played}) do
    num_winning = MapSet.intersection(winning, played) |> MapSet.size()
    {id, num_winning}
  end

  defp parse_line(line) do
    [id, rest] = Regex.run(@card_pattern, line, capture: :all_but_first)

    [winning, played] =
      rest
      |> String.split(" | ", trim: true)
      |> Enum.map(&String.split(&1, " ", trim: true))
      |> Enum.map(&parse_num_list/1)

    {String.to_integer(id), winning, played}
  end

  defp parse_num_list(list) do
    list |> Enum.map(&String.to_integer/1) |> MapSet.new()
  end
end
