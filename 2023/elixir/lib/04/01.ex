defmodule Scratchcards do
  @moduledoc """
  https://adventofcode.com/2023/day/4
  """

  @card_pattern ~r/^Card\s+(\d+): (.*)$/

  def total_points(filename \\ "inputs/04/input.txt") do
    filename
    |> File.stream!()
    |> Stream.map(&String.trim/1)
    |> Stream.map(&parse_line/1)
    |> Stream.map(&count_wins/1)
    |> Stream.map(&calculate_score/1)
    |> Enum.sum()
  end

  defp calculate_score({_, 0}), do: 0
  defp calculate_score({_, num_winning}), do: 2 ** (num_winning - 1)

  defp count_wins({id, winning, played}) do
    num_winning = MapSet.intersection(winning, played) |> MapSet.size()
    {id, num_winning}
  end

  defp parse_line(line) do
    [id, rest] = Regex.run(@card_pattern, line, capture: :all_but_first)

    [winning, played] =
      rest
      |> String.split(" | ", trim: true)
      |> Enum.map(&String.split(&1, " ", trim: true))
      |> Enum.map(&parse_num_list/1)

    {String.to_integer(id), winning, played}
  end

  defp parse_num_list(list) do
    list |> Enum.map(&String.to_integer/1) |> MapSet.new()
  end
end
