defmodule Trebuchet2 do
  @moduledoc """
  https://adventofcode.com/2023/day/1
  """

  def run(filename \\ "inputs/01/input.txt") do
    calibrate(filename)
  end

  @number_mapping %{
    "one" => "1",
    "two" => "2",
    "three" => "3",
    "four" => "4",
    "five" => "5",
    "six" => "6",
    "seven" => "7",
    "eight" => "8",
    "nine" => "9",
  }
  @stringers Map.keys(@number_mapping)

  defp calibrate(filename) do
    {:ok, contents} = File.read(filename)
    contents |> String.split("\n", trim: true) |> Enum.map(&decode_line/1) |> Enum.sum()
  end

  def decode_line(text) do
    numbers =
      text
      |> enumerate_substrings()
      |> Enum.map(&check_substring/1)
      |> Enum.reject(&is_nil/1)

    [List.first(numbers), List.last(numbers)] |> List.to_string() |> String.to_integer()
  end

  defp check_substring(string) do
    match = @stringers |> Enum.find(&String.starts_with?(string, &1))

    case match do
      nil -> string |> String.to_charlist() |> List.first() |> is_ascii_number()
      val -> Map.get(@number_mapping, val)
    end
  end

  defp is_ascii_number(c) do
    case c do
      x when x in 48..57 -> List.to_string([c])
      _ -> nil
    end
  end

  defp enumerate_substrings(string) do
    [string | enumerate_substrings(string, String.length(string) - 1, [])]
  end

  defp enumerate_substrings(_, 0, acc), do: acc

  defp enumerate_substrings(string, n, acc) do
    {prefix, rest} = String.split_at(string, n)
    new_acc = if prefix == "", do: acc, else: [rest | acc]
    enumerate_substrings(string, n - 1, new_acc)
  end
end
