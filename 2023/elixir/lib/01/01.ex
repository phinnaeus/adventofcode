defmodule Trebuchet do
  @moduledoc """
  https://adventofcode.com/2023/day/1
  """

  def run(filename \\ "inputs/01/input.txt") do
    calibrate(filename)
  end

  defp calibrate(filename) do
    {:ok, contents} = File.read(filename)
    contents |> String.split("\n", trim: true) |> Enum.map(&decode_line/1) |> Enum.sum()
  end

  defp decode_line(text) do
    numbers = String.to_charlist(text) |> Enum.filter(&is_ascii_number/1)
    [List.first(numbers), List.last(numbers)] |> List.to_string() |> String.to_integer()
  end

  defp is_ascii_number(c) do
    c >= 48 && c <= 57
  end
end
