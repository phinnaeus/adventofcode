defmodule Mix.Tasks.NewDay do
  @moduledoc """
  Scaffold a new Advent of Code puzzle day
  mix new_day <PuzzleName>
  """
  use Mix.Task

  @impl Mix.Task
  def run(args) do
    [name] = args
    Mix.shell().info("Creating scaffolding for day #{name}...")

    date_num =
      File.ls!("inputs")
      |> Enum.map(&String.to_integer/1)
      |> Enum.max()
      |> then(fn e -> e + 1 end)
      |> Integer.to_string()
      |> String.pad_leading(2, "0")

    Mix.shell().info("Using date_num #{date_num}...")

    impl_content = EEx.eval_file("lib/mix/templates/impl.eex", name: name, date_num: date_num)
    test_content = EEx.eval_file("lib/mix/templates/test.eex", name: name, date_num: date_num)

    File.mkdir_p!("inputs/#{date_num}")
    File.write!("inputs/#{date_num}/sample.txt", "")

    File.mkdir_p!("lib/#{date_num}")
    File.write!("lib/#{date_num}/01.ex", impl_content)

    File.write!("test/day#{date_num}_test.exs", test_content)
  end
end
