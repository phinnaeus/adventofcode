defmodule BoatRaces2 do
  @moduledoc """
  https://adventofcode.com/2023/day/6
  """

  @time_pattern ~r/^(?:Time:\s+)(.*)$/
  @dist_pattern ~r/^(?:Distance:\s+)(.*)$/

  def ways_to_win_all(filename \\ "inputs/06/input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> parse_input()
    |> ways_to_win_race()
  end

  defp parse_input([time_line, distance_line]) do
    time =
      Regex.run(@time_pattern, time_line, capture: :all_but_first)
      |> List.first()
      |> String.replace(" ", "")
      |> String.to_integer()

    dist =
      Regex.run(@dist_pattern, distance_line, capture: :all_but_first)
      |> List.first()
      |> String.replace(" ", "")
      |> String.to_integer()

    {time, dist}
  end

  defp ways_to_win_race({time, distance}) do
    mid = div(time, 2)

    result =
      mid..0//-1
      |> Enum.reduce_while([], fn e, acc ->
        dist = e * (time - e)
        # IO.puts("for time holding button #{e}, distance = #{dist}")
        if dist > distance do
          {:cont, [e | acc]}
        else
          {:halt, acc}
        end
      end)
      |> length()

    case rem(time, 2) do
      0 -> result * 2 - 1
      1 -> result * 2
    end
  end
end
