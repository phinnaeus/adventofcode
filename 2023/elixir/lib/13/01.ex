defmodule PointOfIncidence do
  @moduledoc """

  https://adventofcode.com/2023/day/13
  """

  def run(filename \\ "inputs/13/input.txt") do
    filename
    |> File.read!()
    |> String.split("\n\n", trim: true)
    |> Enum.map(&parse_image/1)
    |> Enum.map(&find_axis/1)
    |> Enum.map(&summarize/1)
    |> Enum.sum()
  end

  defp summarize({nil, {c, _}}), do: c
  defp summarize({{r, _}, nil}), do: r * 100

  defp find_axis({rows, cols}) do
    {find_axis(rows), find_axis(cols)}
  end

  defp find_axis(elements) do
    {_, _, candidates} =
      elements
      |> Enum.with_index()
      |> Enum.reduce({"", -1, []}, fn {el, i}, {prev, prev_i, acc} ->
        if el == prev do
          {el, i, [{i, prev_i} | acc]}
        else
          {el, i, acc}
        end
      end)

    candidates
    |> Enum.find(&validate_candidate(&1, elements))
  end

  defp validate_candidate({_, 0}, _), do: true
  defp validate_candidate({j, _}, full) when j == length(full) - 1, do: true

  defp validate_candidate({j, i}, full) do
    left = full |> Enum.slice(0..(i - 1)) |> Enum.reverse()
    right = full |> Enum.slice((j + 1)..(length(full) - 1))

    Enum.zip(left, right)
    |> Enum.all?(fn {l, r} -> l == r end)
  end

  defp parse_image(text) do
    rows = String.split(text, "\n", trim: true)

    cols =
      rows
      |> Enum.map(&String.to_charlist/1)
      |> Enum.zip()
      |> Enum.map(&tuple_to_string/1)

    {rows, cols}
  end

  defp tuple_to_string(chars) do
    chars
    |> Tuple.to_list()
    |> List.to_string()
  end
end
