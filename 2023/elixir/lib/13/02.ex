defmodule PointOfIncidence2 do
  @moduledoc """

  https://adventofcode.com/2023/day/13
  """

  def run(filename \\ "inputs/13/input.txt") do
    filename
    |> File.read!()
    |> String.split("\n\n", trim: true)
    |> Enum.map(&parse_image/1)
    |> Enum.map(&find_axis/1)
    |> Enum.map(&summarize/1)
    |> Enum.sum()
  end

  defp summarize({nil, c}), do: c
  defp summarize({r, nil}), do: r * 100

  defp find_axis({rows, cols}) do
    {find_axis(rows), find_axis(cols)}
  end

  defp find_axis(elements) do
    {_, i} =
      elements
      |> Enum.with_index()
      |> Enum.find({nil, nil}, fn
        {_, 0} -> false
        {_, i} -> validate_candidate(i - 1, i, elements)
      end)

    i
  end

  defp validate_candidate(i, j, full) do
    l_range = 0..i
    r_range = j..(length(full) - 1)
    trunc = min(Range.size(l_range), Range.size(r_range))
    left = full |> Enum.slice(l_range) |> Enum.reverse() |> Enum.take(trunc) |> Enum.join(":")
    right = full |> Enum.slice(r_range) |> Enum.take(trunc) |> Enum.join(":")

    compare_smudged_strings(left, right)
  end

  defp compare_smudged_strings(s1, s2) when s1 == s2, do: false

  defp compare_smudged_strings(s1, s2) do
    differences =
      [String.to_charlist(s1), String.to_charlist(s2)]
      |> Enum.zip()
      |> Enum.reject(fn {a, b} -> a == b end)
      |> Enum.count()

    differences == 1
  end

  defp parse_image(text) do
    rows = String.split(text, "\n", trim: true)

    cols =
      rows
      |> Enum.map(&String.to_charlist/1)
      |> Enum.zip()
      |> Enum.map(&tuple_to_string/1)

    {rows, cols}
  end

  defp tuple_to_string(chars) do
    chars
    |> Tuple.to_list()
    |> List.to_string()
  end
end
