defmodule Reflector2 do
  @moduledoc """

  https://adventofcode.com/2023/day/14
  """
  require Stream.Reducers

  @gravity %{
    35 => 0,
    79 => 1,
    46 => 2,
  }

  # north - cols left
  # west - rows left
  # south - cols, reverse, left
  # east, rows, reverse left

  def run(filename \\ "inputs/14/input.txt", cycles \\ 1_000_000_000) do
    cols =
      filename
      |> File.read!()
      |> parse_layout()

    {seen, result} =
      0..(cycles - 1)
      |> Enum.reduce_while({%{}, cols}, fn i, {seen, prev} ->
        new = cycle(prev)

        if Map.has_key?(seen, new) do
          {:halt, {seen, Map.get(seen, new)..i}}
        else
          {:cont, {Map.put_new(seen, new, i), new}}
        end
      end)

    final =
      case result do
        original_i..last_i -> shortcut(seen, original_i..last_i, cycles)
        last -> last
      end

    score(final)
  end

  defp shortcut(seen, range, cycles) do
    start.._ = range
    idx = rem(cycles - start, Range.size(range) - 1) + start - 1

    seen
    |> Enum.find(fn {_, i} -> i == idx end)
    |> elem(0)
  end

  defp cycle(rows) do
    rows
    # |> print_image("north")
    # north
    |> Enum.map(&tilt_left/1)
    |> transpose()
    # west
    |> Enum.map(&tilt_left/1)
    # |> print_image("west")
    |> Enum.reverse()
    |> transpose()
    # south
    |> Enum.map(&tilt_left/1)
    # |> print_image("south")
    |> Enum.reverse()
    |> transpose()
    # east
    |> Enum.map(&tilt_left/1)
    # |> print_image("east")
    |> Enum.reverse()
    |> transpose()
    |> Enum.reverse()

    # |> print_image("north again")
  end

  def print_image(rows, label \\ "") do
    IO.puts("#{label}")
    Enum.each(rows, &IO.puts(&1))
    IO.puts("----------------")
    rows
  end

  defp score(result) do
    result
    # to get the scoring right
    |> transpose()
    |> Enum.reverse()
    |> Enum.with_index()
    |> Enum.map(&score_row/1)
    |> Enum.sum()
  end

  defp score_row({row_cl, row}) do
    num_rocks =
      row_cl
      |> Enum.frequencies()
      |> Map.get(79, 0)

    num_rocks * (row + 1)
  end

  defp tilt_left(row) do
    chunk_fun = fn element, acc ->
      # not rock
      if element == 46 || element == 79 do
        {:cont, [element | acc]}
      else
        {:cont, Enum.reverse(acc), [element]}
      end
    end

    after_fun = fn
      [] -> {:cont, []}
      acc -> {:cont, Enum.reverse(acc), []}
    end

    row
    |> Enum.chunk_while([], chunk_fun, after_fun)
    |> Enum.flat_map(&tilt_chunk/1)
  end

  def tilt_chunk(chunk) do
    Enum.sort(chunk, fn a, b ->
      # TODO how to make this actually error
      a_i = Map.get(@gravity, a, :error)
      b_i = Map.get(@gravity, b, :error)
      a_i <= b_i
    end)
  end

  defp transpose(rows) do
    rows
    |> Enum.zip()
    |> Enum.map(&Tuple.to_list/1)
  end

  defp parse_layout(contents) do
    contents
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_charlist/1)
    |> transpose()
  end
end
