defmodule Reflector do
  @moduledoc """

  https://adventofcode.com/2023/day/14
  """

  @gravity %{
    35 => 0,
    79 => 1,
    46 => 2,
  }

  def run(filename \\ "inputs/14/input.txt") do
    filename
    |> File.read!()
    |> parse_layout()
    |> Enum.map(&tilt_left/1)
    |> Enum.zip()
    # to get the scoring right
    |> Enum.reverse()
    |> Enum.with_index()
    |> Enum.map(&score_row/1)
    |> Enum.sum()
  end

  defp score_row({row_cl, row}) do
    num_rocks =
      row_cl
      |> Tuple.to_list()
      |> Enum.frequencies()
      |> Map.get(79, 0)

    num_rocks * (row + 1)
  end

  defp tilt_left(row) do
    chunk_fun = fn element, acc ->
      # not rock
      if element == 46 || element == 79 do
        {:cont, [element | acc]}
      else
        {:cont, Enum.reverse(acc), [element]}
      end
    end

    after_fun = fn
      [] -> {:cont, []}
      acc -> {:cont, Enum.reverse(acc), []}
    end

    row
    |> String.to_charlist()
    |> Enum.chunk_while([], chunk_fun, after_fun)
    |> Enum.flat_map(&tilt_chunk/1)
  end

  def tilt_chunk(chunk) do
    Enum.sort(chunk, fn a, b ->
      # TODO how to make this actually error
      a_i = Map.get(@gravity, a, :error)
      b_i = Map.get(@gravity, b, :error)
      a_i <= b_i
    end)
  end

  defp parse_layout(contents) do
    contents
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_charlist/1)
    |> Enum.zip()
    |> Enum.map(&tuple_to_string/1)
  end

  defp tuple_to_string(chars) do
    chars
    |> Tuple.to_list()
    |> List.to_string()
  end
end
