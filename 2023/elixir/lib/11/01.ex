defmodule CosmicExpansion do
  @moduledoc """

  https://adventofcode.com/2023/day/11
  """

  def run(filename \\ "inputs/11/input.txt", hubble_const \\ 2) do
    {galaxies, height, width} =
      filename
      |> File.read!()
      |> parse_map()

    {galaxy_cols, galaxy_rows} = Enum.unzip(galaxies)
    galaxy_cols = MapSet.new(galaxy_cols)
    galaxy_rows = MapSet.new(galaxy_rows)

    space_cols = Enum.reject(0..(width - 1), fn i -> i in galaxy_cols end) |> Enum.sort()
    space_rows = Enum.reject(0..(height - 1), fn i -> i in galaxy_rows end) |> Enum.sort()

    galaxies = galaxies |> Enum.map(&expand_space(&1, space_rows, space_cols, hubble_const))

    pairs = for i <- galaxies, j <- galaxies, i < j, do: {i, j}

    pairs
    |> Enum.map(&taxicab_distance/1)
    |> Enum.sum()
  end

  defp taxicab_distance({{x1, y1}, {x2, y2}}) do
    abs(x2 - x1) + abs(y2 - y1)
  end

  defp expand_space({x, y}, space_rows, space_cols, hubble_const) do
    col_offset = Enum.find_index(space_cols, fn v -> v > x end) || length(space_cols)
    row_offset = Enum.find_index(space_rows, fn v -> v > y end) || length(space_rows)
    {x + col_offset * (hubble_const - 1), y + row_offset * (hubble_const - 1)}
  end

  defp parse_map(content) do
    grid =
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.to_charlist/1)
      |> Enum.map(&Enum.with_index/1)
      |> Enum.with_index()
      |> Enum.map(&flatten_row/1)

    galaxies =
      grid
      |> List.flatten()
      |> Enum.filter(fn {_, v} -> is_galaxy(v) end)
      |> Enum.map(fn {c, _} -> c end)

    width = grid |> List.first() |> length()
    height = grid |> length()

    {galaxies, height, width}
  end

  defp flatten_row({row, index}) do
    Enum.map(row, fn {v, col} -> {{col, index}, v} end)
  end

  defp is_galaxy(v), do: v == 35
end
