defmodule DesertMap2 do
  @moduledoc """
  https://adventofcode.com/2023/day/8
  """

  def how_many_steps(filename \\ "inputs/08/input.txt") do
    [dirs_str | nodes_strs] =
      filename
      |> File.read!()
      |> String.split("\n", trim: true)

    dirs = parse_dirs(dirs_str)

    map =
      nodes_strs
      |> Enum.map(&parse_node_str/1)
      |> Enum.into(%{})

    starts =
      map
      |> Map.keys()
      |> Enum.filter(&String.ends_with?(&1, "A"))

    # IO.puts("tracking #{Enum.count(starts)} ghosts")

    starts
    |> Enum.map(&trace_ghost(&1, dirs, map))
    |> Enum.reduce(1, &lcm/2)
  end

  defp trace_ghost(id, dirs, map) do
    Enum.reduce_while(dirs, {1, Map.get(map, id)}, fn dir, {cnt, {left, right}} ->
      case dir do
        "L" ->
          if String.ends_with?(left, "Z") do
            {:halt, cnt}
          else
            {:cont, {cnt + 1, Map.get(map, left)}}
          end

        "R" ->
          if String.ends_with?(right, "Z") do
            {:halt, cnt}
          else
            {:cont, {cnt + 1, Map.get(map, right)}}
          end
      end
    end)
  end

  defp gcd(a, 0), do: a
  defp gcd(a, b), do: gcd(b, rem(a, b))

  defp lcm(a, b) when a != 0 and b != 0 do
    div(abs(a * b), gcd(a, b))
  end

  defp lcm(_, _), do: 0

  defp parse_node_str(node_str) do
    [input, children] = String.split(node_str, " = ")

    [left, right] =
      children
      |> String.trim_leading("(")
      |> String.trim_trailing(")")
      |> String.split(", ")

    {input, {left, right}}
  end

  defp parse_dirs(dirs_str) do
    dirs_str |> String.split("", trim: true) |> Stream.cycle()
  end
end
