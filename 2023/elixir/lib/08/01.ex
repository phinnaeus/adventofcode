defmodule DesertMap do
  @moduledoc """
  https://adventofcode.com/2023/day/8
  """

  def how_many_steps(filename \\ "inputs/08/input.txt") do
    [dirs_str | nodes_strs] =
      filename
      |> File.read!()
      |> String.split("\n", trim: true)

    dirs = parse_dirs(dirs_str)

    map =
      nodes_strs
      |> Enum.map(&parse_node_str/1)
      |> Enum.into(%{})

    Enum.reduce_while(dirs, {1, Map.get(map, "AAA")}, fn dir, {cnt, {left, right}} ->
      case {dir, left, right} do
        {"L", "ZZZ", _} -> {:halt, cnt}
        {"L", _, _} -> {:cont, {cnt + 1, Map.get(map, left)}}
        {"R", _, "ZZZ"} -> {:halt, cnt}
        {"R", _, _} -> {:cont, {cnt + 1, Map.get(map, right)}}
      end
    end)
  end

  defp parse_node_str(node_str) do
    [input, children] = String.split(node_str, " = ")

    [left, right] =
      children
      |> String.trim_leading("(")
      |> String.trim_trailing(")")
      |> String.split(", ")

    {input, {left, right}}
  end

  defp parse_dirs(dirs_str) do
    dirs_str |> String.split("", trim: true) |> Stream.cycle()
  end
end
