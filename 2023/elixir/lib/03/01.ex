defmodule Gears do
  @moduledoc """
  https://adventofcode.com/2023/day/3
  """

  def part_numbers(filename \\ "inputs/03/input.txt") do
    {symbols, part_nums, part_ids} =
      filename
      |> File.stream!()
      |> Enum.map(&String.trim/1)
      |> construct_schematic()

    symbols
    |> Enum.map(&symbol_neighbors(&1, part_nums))
    |> List.flatten()
    |> Enum.reject(&is_nil/1)
    |> Enum.uniq()
    |> Enum.map(&Map.get(part_ids, &1))
    |> Enum.sum()
  end

  def gear_ratios(filename \\ "inputs/03/input.txt") do
    {symbols, part_nums, part_ids} =
      filename
      |> File.stream!()
      |> Enum.map(&String.trim/1)
      |> construct_schematic()

    symbols
    |> Enum.filter(fn {_, val} -> is_ascii_asterisk(val) end)
    |> Enum.map(&symbol_neighbors(&1, part_nums))
    |> Enum.map(fn neighbors -> neighbors |> Enum.reject(&is_nil/1) |> Enum.uniq() end)
    |> Enum.filter(fn neighbors -> length(neighbors) == 2 end)
    |> Enum.map(fn gear_ids -> Enum.map(gear_ids, &Map.get(part_ids, &1)) end)
    |> Enum.map(fn [gear1, gear2] -> gear1 * gear2 end)
    |> Enum.sum()
  end

  defp construct_schematic(lines) do
    parsed_lines =
      lines
      |> Enum.with_index(fn el, idx -> {idx, el} end)
      |> Enum.map(&parse_line/1)

    symbols =
      parsed_lines
      |> Enum.map(&elem(&1, 1))
      |> List.flatten()

    part_nums =
      parsed_lines
      |> Enum.map(&elem(&1, 2))
      |> Enum.reduce(fn el, acc -> Map.merge(acc, el) end)

    part_ids =
      parsed_lines
      |> Enum.map(&elem(&1, 3))
      |> Enum.reduce(fn el, acc -> Map.merge(acc, el) end)

    {symbols, part_nums, part_ids}
  end

  defp parse_line({index, line}) do
    filtered =
      line
      |> String.to_charlist()
      |> Enum.with_index(fn el, idx -> {idx, el} end)
      |> Enum.reject(fn {_, val} -> is_ascii_dot(val) end)

    {numbers, symbols} = filtered |> Enum.split_with(fn {_, val} -> is_ascii_number(val) end)

    after_fun = fn
      [] -> {:cont, []}
      acc -> {:cont, Enum.reverse(acc), []}
    end

    part_nums =
      numbers
      |> Enum.chunk_while([], &consecutive_indices?/2, after_fun)
      |> Enum.map(&lump_part_num/1)
      |> List.flatten()

    part_num_ids =
      part_nums
      |> Enum.map(fn {cols, part_num} -> {part_num, coords_based_id(cols, index)} end)
      |> Enum.map(fn {x, y} -> {y, x} end)
      |> Enum.into(%{})

    part_num_id_coords =
      part_nums
      |> Enum.map(&part_num_coord(&1, index))
      |> List.flatten()
      |> Enum.into(%{})

    {
      index,
      Enum.map(symbols, &symbol_coords(&1, index)),
      part_num_id_coords,
      part_num_ids
    }
  end

  defp coords_based_id(cols, row) do
    "#{row}:#{Enum.join(cols, ",")}"
  end

  defp symbol_neighbors({coord, _}, map) do
    coord
    |> neighbor_coords()
    |> Enum.map(&Map.get(map, &1))
  end

  defp neighbor_coords({x, y}) do
    [
      {x - 1, y - 1},
      {x - 1, y},
      {x - 1, y + 1},
      {x, y - 1},
      {x, y + 1},
      {x + 1, y - 1},
      {x + 1, y},
      {x + 1, y + 1},
    ]
  end

  defp consecutive_indices?(el, []), do: {:cont, [el]}

  defp consecutive_indices?(el, [{prev_idx, _} | _] = acc) do
    {el_idx, _} = el

    if el_idx - prev_idx == 1 do
      {:cont, [el | acc]}
    else
      {:cont, Enum.reverse(acc), [el]}
    end
  end

  defp lump_part_num(digit_coords) do
    part_num =
      digit_coords
      |> Enum.map(&elem(&1, 1))
      |> List.to_string()
      |> String.to_integer()

    coords = Enum.map(digit_coords, &elem(&1, 0))
    {coords, part_num}
  end

  defp symbol_coords({col, val}, row), do: {{row, col}, val}

  defp part_num_coord({cols, _}, row) do
    for col <- cols do
      {{row, col}, coords_based_id(cols, row)}
    end
  end

  defp is_ascii_asterisk(c) do
    c == 42
  end

  defp is_ascii_dot(c) do
    c == 46
  end

  defp is_ascii_number(c) do
    c >= 48 && c <= 57
  end
end
