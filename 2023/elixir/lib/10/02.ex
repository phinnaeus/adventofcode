defmodule PipeMaze2 do
  @moduledoc """
  https://adventofcode.com/2023/day/10
  """

  #   1
  # 0 . 2
  #   3
  @incoming_connections %{
    0 => ["-", "F", "L", "S"],
    1 => ["|", "7", "F", "S"],
    2 => ["-", "7", "J", "S"],
    3 => ["|", "J", "L", "S"],
  }

  @outgoing_connections %{
    "S" => [0, 1, 2, 3],
    "|" => [1, 3],
    "-" => [0, 2],
    "L" => [1, 2],
    "J" => [0, 1],
    "7" => [0, 3],
    "F" => [2, 3],
  }

  def run(filename \\ "inputs/10/input.txt") do
    {maze_tiles, start_coord, height, width} =
      filename
      |> File.read!()
      |> parse_maze()

    {paths, dirs} =
      symbol_neighbors(start_coord, maze_tiles)
      |> Enum.map(&follow_path(&1, maze_tiles, [start_coord]))
      |> Enum.with_index()
      |> Enum.unzip()

    real_start_symb =
      case dirs do
        [1, 3] -> "|"
        [0, 2] -> "-"
        [1, 2] -> "7"
        [0, 1] -> "F"
        [0, 3] -> "L"
        [2, 3] -> "J"
      end

    {start_loc, "S"} = start_coord

    golden_path =
      paths
      |> List.first()
      |> Enum.into(%{})
      |> Map.replace(start_loc, real_start_symb)

    all_coords =
      for y <- 0..(height - 1) do
        for x <- 0..(width - 1) do
          {x, y}
        end
      end

    all_coords
    |> Enum.map(&check_fill_for_row(&1, golden_path))
    |> Enum.sum()
  end

  defp check_fill_for_row(row, golden_tiles) do
    {count, _} =
      row
      |> Enum.map(&Map.get(golden_tiles, &1, "."))
      |> Enum.reduce({0, false}, fn tile, {inside_count, parity} ->
        case {tile, parity} do
          {".", true} -> {inside_count + 1, true}
          {".", false} -> {inside_count, false}
          {tile, parity} when tile in ["|", "F", "7"] -> {inside_count, !parity}
          {_, parity} -> {inside_count, parity}
        end
      end)

    count
  end

  defp follow_path({_, "S"}, _, path), do: path
  defp follow_path({_, "."}, _, _), do: []

  defp follow_path(coord, map, [prev | path]) do
    symbol_neighbors(coord, map)
    |> Enum.filter(fn c -> c != prev end)
    |> Enum.flat_map(&follow_path(&1, map, [coord, prev] ++ path))
  end

  defp symbol_neighbors({coord, v}, map) do
    all_neighbors =
      coord
      |> neighbor_coords()
      |> Enum.map(fn coord -> {coord, Map.get(map, coord, ".")} end)

    valid_out = Map.get(@outgoing_connections, v)

    all_neighbors
    |> Enum.with_index()
    |> Enum.filter(fn {{_, v}, i} -> i in valid_out && v in Map.get(@incoming_connections, i) end)
    |> Enum.map(fn {v, _} -> v end)
  end

  defp neighbor_coords({x, y}) do
    [
      {x - 1, y},
      {x, y - 1},
      {x + 1, y},
      {x, y + 1},
    ]
  end

  defp parse_maze(content) do
    grid =
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.to_charlist/1)
      |> Enum.map(&Enum.with_index/1)
      |> Enum.with_index()
      |> Enum.map(&flatten_row/1)

    all_tiles =
      grid
      |> List.flatten()
      |> Enum.map(fn {c, v} -> {c, List.to_string([v])} end)

    start = Enum.find(all_tiles, fn {_, v} -> v == "S" end)

    width = grid |> List.first() |> length()
    height = grid |> length()

    {Map.new(all_tiles), start, height, width}
  end

  defp flatten_row({row, index}) do
    Enum.map(row, fn {v, col} -> {{col, index}, v} end)
  end
end
