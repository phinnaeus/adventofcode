defmodule PipeMaze do
  @moduledoc """
  https://adventofcode.com/2023/day/10
  """

  def run(filename \\ "inputs/10/input.txt") do
    {maze_tiles, start_coord} =
      filename
      |> File.read!()
      |> parse_maze()

    max_dist =
      symbol_neighbors(start_coord, maze_tiles)
      |> Enum.map(&follow_path(nil, &1, maze_tiles, 1))
      |> List.first()

    div(max_dist, 2)
  end

  defp follow_path(_, {_, "S"}, _, count), do: count
  defp follow_path(_, {_, "."}, _, _), do: 0

  defp follow_path(prev, coord, map, count) do
    symbol_neighbors(coord, map)
    |> Enum.filter(fn c -> c != prev end)
    |> Enum.map(&follow_path(coord, &1, map, count + 1))
    |> Enum.max()
  end

  defp symbol_neighbors({coord, v}, map) do
    all_neighbors =
      coord
      |> neighbor_coords()
      |> Enum.map(fn coord -> {coord, Map.get(map, coord, ".")} end)

    #   1
    # 0 . 2
    #   3
    idxs =
      case v do
        "S" -> [0, 1, 2, 3]
        "|" -> [1, 3]
        "-" -> [0, 2]
        "L" -> [1, 2]
        "J" -> [0, 1]
        "7" -> [0, 3]
        "F" -> [2, 3]
      end

    all_neighbors
    |> Enum.with_index()
    |> Enum.filter(fn {_, i} -> i in idxs end)
    |> Enum.map(fn {v, _} -> v end)
    |> Enum.reject(fn {_, v} -> v == "." end)
  end

  defp neighbor_coords({x, y}) do
    [
      {x - 1, y},
      {x, y - 1},
      {x + 1, y},
      {x, y + 1},
    ]
  end

  defp parse_maze(content) do
    all_tiles =
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.to_charlist/1)
      |> Enum.map(&Enum.with_index/1)
      |> Enum.with_index()
      |> Enum.map(&flatten_row/1)
      |> List.flatten()
      |> Enum.map(fn {c, v} -> {c, List.to_string([v])} end)

    {Map.new(all_tiles), Enum.find(all_tiles, fn {_, v} -> v == "S" end)}
  end

  defp flatten_row({row, index}) do
    Enum.map(row, fn {v, col} -> {{col, index}, v} end)
  end
end
