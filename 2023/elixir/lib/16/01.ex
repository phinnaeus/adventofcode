defmodule Lava do
  @moduledoc """

  https://adventofcode.com/2023/day/16
  """

  #  1      1
  # 0 2 -> 0 2
  #  3      3
  @tiles %{
    "|" => {[1, 3], [3], [3, 1], [1]},
    "-" => {[2], [0, 2], [0], [2, 0]},
    "/" => {[1], [0], [3], [2]},
    "\\" => {[3], [2], [1], [0]},
    "." => {[2], [3], [0], [1]},
  }

  def run(filename \\ "inputs/16/input.txt") do
    {all_tiles, _height, _width} =
      filename
      |> File.read!()
      |> parse_maze()

    visited = search_maze(MapSet.new(), all_tiles, [{{0, 0}, 2}])

    unique =
      visited
      |> Enum.map(fn {coord, _} -> coord end)
      |> Enum.uniq()

    # |> IO.inspect()

    # print_result(unique, height, width)

    Enum.count(unique)
  end

  def print_result(visited, height, width) do
    for y <- 0..(height - 1) do
      for x <- 0..(width - 1) do
        if {x, y} in visited do
          IO.write("#")
        else
          IO.write(".")
        end
      end

      IO.write("\n")
    end
  end

  defp search_maze(graph, _, []), do: graph

  defp search_maze(graph, all_tiles, [{coord, dir} | remaining]) do
    # IO.inspect(coord, label: "current coordinate")
    # IO.inspect(dir, label: "current heading")
    neighbors = neighbor_coords(coord)

    new_dirs =
      all_tiles
      |> Map.get(coord)
      |> get_next_tiles(dir, neighbors)
      # |> IO.inspect()
      |> Enum.reject(&MapSet.member?(graph, &1))
      |> Enum.filter(fn {coord, _} -> Map.has_key?(all_tiles, coord) end)

    # |> IO.inspect()

    # TODO make this into a pattern match somehow
    search_maze(MapSet.put(graph, {coord, dir}), all_tiles, remaining ++ new_dirs)
  end

  defp get_next_tiles(nil, _, _), do: []

  defp get_next_tiles(tile, source_dir, neighbors) do
    # source dir is the direction we were going in the previous tile
    new_dir = rem(source_dir + 2, 4)

    out_dirs =
      @tiles
      |> Map.get(tile)
      |> elem(new_dir)

    # |> IO.inspect(label: "directions we can go now")

    neighbors
    # |> IO.inspect()
    |> Enum.with_index()
    |> Enum.map(fn {nb, i} ->
      if i in out_dirs do
        {nb, i}
      end
    end)
    # |> IO.inspect()
    |> Enum.reject(&is_nil/1)
  end

  defp neighbor_coords({x, y}) do
    [
      {x - 1, y},
      {x, y - 1},
      {x + 1, y},
      {x, y + 1},
    ]
  end

  defp parse_maze(content) do
    grid =
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.to_charlist/1)
      |> Enum.map(&Enum.with_index/1)
      |> Enum.with_index()
      |> Enum.map(&flatten_row/1)

    height = Enum.count(grid)
    width = List.first(grid) |> Enum.count()

    all_tiles =
      grid
      |> List.flatten()
      |> Enum.map(fn {c, v} -> {c, List.to_string([v])} end)
      |> Map.new()

    {all_tiles, height, width}
  end

  defp flatten_row({row, index}) do
    Enum.map(row, fn {v, col} -> {{col, index}, v} end)
  end
end
