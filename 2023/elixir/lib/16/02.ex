defmodule Lava2 do
  @moduledoc """

  https://adventofcode.com/2023/day/16
  """

  #  1      1
  # 0 2 -> 0 2
  #  3      3
  @tiles %{
    "|" => {[1, 3], [3], [3, 1], [1]},
    "-" => {[2], [0, 2], [0], [2, 0]},
    "/" => {[1], [0], [3], [2]},
    "\\" => {[3], [2], [1], [0]},
    "." => {[2], [3], [0], [1]},
  }

  def run(filename \\ "inputs/16/input.txt") do
    {all_tiles, height, width} =
      filename
      |> File.read!()
      |> parse_maze()

    generate_start_coord_dirs(height, width)
    |> Enum.reduce(%{}, fn start, routes ->
      search(all_tiles, routes, start)
    end)
    |> Enum.map(fn {_, visited} -> route_score(visited) end)
    |> Enum.max()
  end

  defp generate_start_coord_dirs(height, width) do
    Enum.map(0..(width - 1), fn x -> {{x, 0}, 3} end) ++
      Enum.map(0..(height - 1), fn y -> {{width - 1, y}, 0} end) ++
      Enum.map(0..(width - 1), fn x -> {{x, height - 1}, 1} end) ++
      Enum.map(0..(height - 1), fn y -> {{0, y}, 2} end)
  end

  defp search(all_tiles, routes, start) do
    visited = search_maze(MapSet.new(), routes, all_tiles, [start])
    Map.put(routes, start, visited)
  end

  defp route_score(visited_coord_dirs) do
    visited_coord_dirs
    |> Enum.map(fn {coord, _} -> coord end)
    |> Enum.uniq()
    |> Enum.count()
  end

  defp search_maze(visited, _, _, []), do: visited

  defp search_maze(visited, routes, all_tiles, [{coord, dir} | remaining]) do
    if Map.has_key?(routes, {coord, dir}) do
      {x, y} = coord
      IO.puts("found shortcut at (#{x}, #{y}) heading #{dir}")
      # TODO might need to have this for every visited cell, not just the starts
      MapSet.union(visited, Map.get(routes, {coord, dir}))
    else
      neighbors = neighbor_coords(coord)

      new_dirs =
        all_tiles
        |> Map.get(coord)
        |> get_next_tiles(dir, neighbors)
        |> Enum.reject(&MapSet.member?(visited, &1))
        |> Enum.filter(fn {coord, _} -> Map.has_key?(all_tiles, coord) end)

      # TODO make this into a pattern match somehow
      search_maze(MapSet.put(visited, {coord, dir}), routes, all_tiles, remaining ++ new_dirs)
    end
  end

  defp get_next_tiles(nil, _, _), do: []

  defp get_next_tiles(tile, source_dir, neighbors) do
    # source dir is the direction we were going in the previous tile
    new_dir = rem(source_dir + 2, 4)

    out_dirs =
      @tiles
      |> Map.get(tile)
      |> elem(new_dir)

    # |> IO.inspect(label: "directions we can go now")

    neighbors
    # |> IO.inspect()
    |> Enum.with_index()
    |> Enum.map(fn {nb, i} ->
      if i in out_dirs do
        {nb, i}
      end
    end)
    # |> IO.inspect()
    |> Enum.reject(&is_nil/1)
  end

  defp neighbor_coords({x, y}) do
    [
      {x - 1, y},
      {x, y - 1},
      {x + 1, y},
      {x, y + 1},
    ]
  end

  defp parse_maze(content) do
    grid =
      content
      |> String.split("\n", trim: true)
      |> Enum.map(&String.to_charlist/1)
      |> Enum.map(&Enum.with_index/1)
      |> Enum.with_index()
      |> Enum.map(&flatten_row/1)

    height = Enum.count(grid)
    width = List.first(grid) |> Enum.count()

    all_tiles =
      grid
      |> List.flatten()
      |> Enum.map(fn {c, v} -> {c, List.to_string([v])} end)
      |> Map.new()

    {all_tiles, height, width}
  end

  defp flatten_row({row, index}) do
    Enum.map(row, fn {v, col} -> {{col, index}, v} end)
  end
end
