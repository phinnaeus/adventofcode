defmodule LensLibrary do
  @moduledoc """

  https://adventofcode.com/2023/day/15
  """

  def run(filename \\ "inputs/15/input.txt") do
    filename
    |> File.read!()
    |> String.replace("\n", ",")
    |> String.split(",", trim: true)
    |> Enum.map(&hash/1)
    |> Enum.sum()
  end

  defp hash(string) do
    string
    |> String.to_charlist()
    |> Enum.reduce(0, fn c, acc ->
      acc
      |> Kernel.+(c)
      |> Kernel.*(17)
      |> rem(256)
    end)
  end
end
