defmodule LensLibrary2 do
  @moduledoc """

  https://adventofcode.com/2023/day/15
  """

  @step_pattern ~r/([a-z]+)(-|=)([0-9])?/

  def run(filename \\ "inputs/15/input.txt") do
    filename
    |> File.read!()
    |> String.replace("\n", ",")
    |> String.split(",", trim: true)
    |> Enum.reduce(%{}, &parse_step/2)
    |> Enum.map(&focusing_power/1)
    |> Enum.sum()
  end

  defp focusing_power({box_num, box}) do
    box
    |> Enum.reverse()
    |> Enum.with_index(1)
    |> Enum.reduce(0, fn {{_, fl}, i}, acc -> acc + (box_num + 1) * fl * i end)
  end

  defp parse_step(step, lenses) do
    [label | rest] = Regex.run(@step_pattern, step, capture: :all_but_first)
    box = hash(label)

    case rest do
      ["-"] -> shift_boxes(lenses, box, label)
      ["=", focal_length] -> add_lens(lenses, box, label, focal_length)
    end
  end

  defp shift_boxes(lenses, box_id, label) do
    Map.replace_lazy(lenses, box_id, fn list ->
      List.keydelete(list, label, 0)
    end)
  end

  defp add_lens(lenses, box_id, label, focal_length) do
    new = {label, String.to_integer(focal_length)}

    Map.update(lenses, box_id, [new], fn list ->
      if List.keymember?(list, label, 0) do
        List.keyreplace(list, label, 0, new)
      else
        [new | list]
      end
    end)
  end

  defp hash(string) do
    string
    |> String.to_charlist()
    |> Enum.reduce(0, fn c, acc ->
      acc
      |> Kernel.+(c)
      |> Kernel.*(17)
      |> rem(256)
    end)
  end
end
