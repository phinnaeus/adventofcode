defmodule SnowIsland do
  @moduledoc """
  https://adventofcode.com/2023/day/2
  """

  def run(filename \\ "inputs/02/input.txt") do
    which_games_possible(filename, 12, 13, 14)
  end

  @game_pattern ~r/^Game (\d+): (.*)$/

  defp which_games_possible(filename, red, green, blue) do
    File.stream!(filename)
    |> Enum.map(&String.trim/1)
    |> Enum.map(&game_result_value(&1, red, green, blue))
    |> Enum.sum()
  end

  defp game_result_value(game, red, green, blue) do
    [id, rest] = Regex.run(@game_pattern, game, capture: :all_but_first)
    rounds = rest |> String.split("; ") |> Enum.map(&parse_draw_set/1)

    red_ok = rounds |> Enum.all?(&possible_round?(&1, :red, red))
    green_ok = rounds |> Enum.all?(&possible_round?(&1, :green, green))
    blue_ok = rounds |> Enum.all?(&possible_round?(&1, :blue, blue))

    if red_ok && green_ok && blue_ok do
      String.to_integer(id)
    else
      0
    end
  end

  defp possible_round?(round, color, count) do
    Map.get(round, color, 0) <= count
  end

  defp parse_draw_set(repr) do
    repr |> String.split(", ") |> Enum.map(&parse_color_count/1) |> Map.new()
  end

  defp parse_color_count(repr) do
    parts = String.split(repr, " ")
    {List.last(parts) |> String.to_atom(), List.first(parts) |> String.to_integer()}
  end
end
