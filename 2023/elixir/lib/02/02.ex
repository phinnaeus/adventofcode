defmodule SnowIsland2 do
  @moduledoc """
  https://adventofcode.com/2023/day/2
  """

  def run(filename \\ "inputs/02/input.txt") do
    cumulative_power(filename)
  end

  @game_pattern ~r/^Game (?:\d+): (.*)$/

  defp cumulative_power(filename) do
    File.stream!(filename)
    |> Enum.map(&String.trim/1)
    |> Enum.map(&game_power/1)
    |> Enum.sum()
  end

  defp game_power(game) do
    [rest] = Regex.run(@game_pattern, game, capture: :all_but_first)
    rounds = rest |> String.split("; ") |> Enum.map(&parse_draw_set/1)
    # i am certain there is a simpler way to write this
    min_red = rounds |> Enum.map(&Map.get(&1, :red, 0)) |> Enum.max()
    min_green = rounds |> Enum.map(&Map.get(&1, :green, 0)) |> Enum.max()
    min_blue = rounds |> Enum.map(&Map.get(&1, :blue, 0)) |> Enum.max()
    min_red * min_green * min_blue
  end

  defp parse_draw_set(repr) do
    repr |> String.split(", ") |> Enum.map(&parse_color_count/1) |> Map.new()
  end

  defp parse_color_count(repr) do
    parts = String.split(repr, " ")
    {List.last(parts) |> String.to_atom(), List.first(parts) |> String.to_integer()}
  end
end
