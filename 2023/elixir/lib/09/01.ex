defmodule OasisReport do
  @moduledoc """
  https://adventofcode.com/2023/day/9
  """

  def extrapolated_value_sum(filename \\ "inputs/09/input.txt", prev \\ false) do
    readings =
      filename
      |> File.stream!()
      |> Stream.map(&String.trim/1)
      |> Stream.map(&String.split(&1, " ", trim: true))
      |> Stream.map(&Enum.map(&1, fn s -> String.to_integer(s) end))

    if prev do
      readings
      |> Stream.map(&Enum.reverse/1)
      |> Stream.map(&predict_next_value/1)
      |> Enum.sum()
    else
      readings
      |> Stream.map(&predict_next_value/1)
      |> Enum.sum()
    end
  end

  defp predict_next_value(readings) do
    readings
    |> Stream.unfold(fn nums ->
      if Enum.all?(nums, &(&1 == 0)) do
        nil
      else
        {nums, derivative(nums)}
      end
    end)
    |> Enum.reverse()
    |> Enum.reduce(fn
      e, [_ | tail] -> List.last(tail) + List.last(e)
      e, acc -> acc + List.last(e)
    end)
  end

  defp derivative(values) do
    values
    |> Enum.reduce({nil, []}, fn
      e, {nil, acc} -> {e, acc}
      e, {prev, acc} -> {e, [e - prev | acc]}
    end)
    |> elem(1)
    |> Enum.reverse()
  end
end
