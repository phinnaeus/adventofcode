defmodule CamelCards2 do
  @moduledoc """
  https://adventofcode.com/2023/day/7
  """

  @cards %{
    "A" => 13,
    "K" => 12,
    "Q" => 11,
    "T" => 9,
    "9" => 8,
    "8" => 7,
    "7" => 6,
    "6" => 5,
    "5" => 4,
    "4" => 3,
    "3" => 2,
    "2" => 1,
    "J" => 0,
  }

  def total_winnings(filename \\ "inputs/07/input.txt") do
    filename
    |> File.stream!()
    |> Enum.map(&parse_line/1)
    |> Enum.sort(fn {h1, _}, {h2, _} ->
      compare_hands(h1, h2)
    end)
    |> Enum.reverse()
    |> Enum.with_index(1)
    |> Enum.reduce(0, fn {{_, bid}, rank}, acc -> acc + bid * rank end)
  end

  defp compare_hands(h1, h2) do
    score1 = hand_score(h1)
    score2 = hand_score(h2)

    if score1 == score2 do
      # second level sort
      Enum.zip(h1, h2)
      |> Enum.reduce_while(nil, fn {c1, c2}, acc ->
        s1 = Map.get(@cards, c1, 0)
        s2 = Map.get(@cards, c2, 0)

        if s1 == s2 do
          {:cont, acc}
        else
          {:halt, s1 > s2}
        end
      end)
    else
      score1 > score2
    end
  end

  # first level sort
  defp hand_score(hand) do
    hand_set_freq = Enum.frequencies(hand)
    j_count = Map.get(hand_set_freq, "J", 0)

    hand_set =
      if j_count > 0 do
        case hand_set_freq |> Map.delete("J") |> Map.values() |> Enum.sort(:desc) do
          [top | rest] -> [top + j_count | rest]
          [] -> [5]
        end
      else
        Map.values(hand_set_freq) |> Enum.sort(:desc)
      end

    case hand_set do
      [5] -> 6
      [4, 1] -> 5
      [3, 2] -> 4
      [3, 1, 1] -> 3
      [2, 2, 1] -> 2
      [2, 1, 1, 1] -> 1
      [1, 1, 1, 1, 1] -> 0
    end
  end

  defp parse_line(line) do
    [hand, bid] = String.split(line, " ", trim: true)

    {
      String.split(hand, "", trim: true),
      String.trim(bid) |> String.to_integer()
    }
  end
end
