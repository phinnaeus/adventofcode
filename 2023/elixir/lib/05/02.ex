defmodule Seeds2 do
  @moduledoc """
  https://adventofcode.com/2023/day/5
  """

  @almanac_pattern ~r/(?:seeds:\s+)([\d ]+)\s+
(?:seed-to-soil map:\s+)([\d\s]+)\s+
(?:soil-to-fertilizer map:\s+)([\d\s]+)\s+
(?:fertilizer-to-water map:\s+)([\d\s]+)\s+
(?:water-to-light map:\s+)([\d\s]+)\s+
(?:light-to-temperature map:\s+)([\d\s]+)\s+
(?:temperature-to-humidity map:\s+)([\d\s]+)\s+
(?:humidity-to-location map:\s+)([\d\s]+)/

  def lowest_seed_location(filename \\ "inputs/05/input.txt") do
    almanac_text = File.read!(filename)

    [
      seeds_str,
      seed_to_soil_str,
      soil_to_fert_str,
      fert_to_water_str,
      water_to_light_str,
      light_to_temp_str,
      temp_to_hum_str,
      hum_to_loc_str,
    ] = Regex.run(@almanac_pattern, almanac_text, capture: :all_but_first)

    seeds =
      seeds_str
      |> String.split(" ", trim: true)
      |> Enum.map(&String.to_integer/1)
      |> Enum.chunk_every(2)
      |> Enum.map(fn [start, len] -> start..(start + len - 1) end)
      |> List.flatten()

    # total_seeds =
    #   seeds
    #   |> Enum.reduce(0, fn s, acc -> acc + Range.size(s) end)
    #   |> Integer.to_string()
    #   |> String.pad_leading(3, "0")

    # IO.puts("total seeds: #{total_seeds} in #{length(seeds)} ranges")

    conversion_maps =
      [
        seed_to_soil_str,
        soil_to_fert_str,
        fert_to_water_str,
        water_to_light_str,
        light_to_temp_str,
        temp_to_hum_str,
        hum_to_loc_str,
      ]
      |> Enum.reverse()
      |> Enum.map(&parse_map_str/1)
      |> Enum.map(&Map.new/1)

    Stream.iterate(0, &(&1 + 1))
    |> Enum.find(fn loc ->
      seed = convert_with_all_maps(loc, conversion_maps)
      # IO.puts("checking location #{loc} with candidate seed #{seed}...")
      Enum.any?(seeds, &Enum.member?(&1, seed))
    end)
  end

  defp convert_with_all_maps(input, maps) do
    Enum.reduce(maps, input, &convert_with_map/2)
  end

  defp convert_with_map(map, input) do
    usable_map = Enum.find(map, fn {range, _} -> Enum.member?(range, input) end)

    case usable_map do
      {_, offset} -> input + offset
      nil -> input
    end
  end

  defp parse_map_str(map_str) do
    map_str
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_range_str/1)
  end

  # Each line within a map contains three numbers:
  # the destination range start
  # the source range start
  # the range length.
  defp parse_range_str(range_str) do
    [dest_start, source_start, len] =
      range_str
      |> String.split(" ", trim: true)
      |> Enum.map(&String.to_integer/1)

    {dest_start..(dest_start + len - 1), source_start - dest_start}
  end
end
