defmodule ScratchcardsTest do
  use ExUnit.Case

  test "part 1" do
    assert Scratchcards.total_points("inputs/04/sample.txt") == 13
  end

  test "part 2" do
    assert Scratchcards2.total_cards("inputs/04/sample.txt") == 30
  end
end
