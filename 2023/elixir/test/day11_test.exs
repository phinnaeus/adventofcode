defmodule CosmicExpansionTest do
  use ExUnit.Case

  test "part 1" do
    assert CosmicExpansion.run("inputs/11/sample.txt") == 374
  end

  test "part 2" do
    assert CosmicExpansion.run("inputs/11/sample.txt", 10) == 1030
    assert CosmicExpansion.run("inputs/11/sample.txt", 100) == 8410
  end
end
