defmodule LavaTest do
  use ExUnit.Case

  test "part 1" do
    assert Lava.run("inputs/16/sample.txt") == 46
  end

  test "part 2" do
    assert Lava2.run("inputs/16/sample.txt") == 51
  end
end
