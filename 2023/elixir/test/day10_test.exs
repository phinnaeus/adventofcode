defmodule PipeMazeTest do
  use ExUnit.Case

  test "part 1" do
    assert PipeMaze.run("inputs/10/sample.txt") == 4
    assert PipeMaze.run("inputs/10/sample2.txt") == 8
  end

  test "part 2" do
    assert PipeMaze2.run("inputs/10/sample3.txt") == 4
    assert PipeMaze2.run("inputs/10/sample4.txt") == 5
    assert PipeMaze2.run("inputs/10/sample5.txt") == 8
    assert PipeMaze2.run("inputs/10/sample6.txt") == 10
  end
end
