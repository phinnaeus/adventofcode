defmodule GearsTest do
  use ExUnit.Case

  test "part 1" do
    assert Gears.part_numbers("inputs/03/sample.txt") == 4361
  end

  test "part 2" do
    assert Gears.gear_ratios("inputs/03/sample.txt") == 467_835
  end
end
