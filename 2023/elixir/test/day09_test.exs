defmodule OasisReportTest do
  use ExUnit.Case

  test "part 1" do
    assert OasisReport.extrapolated_value_sum("inputs/09/sample.txt") == 114
  end

  test "part 2" do
    assert OasisReport.extrapolated_value_sum("inputs/09/sample.txt", true) == 2
  end
end
