defmodule SnowIslandTest do
  use ExUnit.Case

  test "part 1" do
    assert SnowIsland.run("inputs/02/sample.txt") == 8
  end

  test "part 2" do
    assert SnowIsland2.run("inputs/02/sample.txt") == 2286
  end
end
