defmodule SeedsTest do
  use ExUnit.Case

  test "part 1" do
    assert Seeds.lowest_seed_location("inputs/05/sample.txt") == 35
  end

  test "part 2" do
    assert Seeds2.lowest_seed_location("inputs/05/sample.txt") == 46
  end
end
