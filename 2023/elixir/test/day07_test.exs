defmodule CamelCardsTest do
  use ExUnit.Case

  test "part 1" do
    assert CamelCards.total_winnings("inputs/07/sample.txt") == 6440
  end

  test "part 2" do
    assert CamelCards2.total_winnings("inputs/07/sample.txt") == 5905
  end
end
