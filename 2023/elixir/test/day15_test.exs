defmodule LensLibraryTest do
  use ExUnit.Case

  test "part 1" do
    assert LensLibrary.run("inputs/15/hash.txt") == 52
    assert LensLibrary.run("inputs/15/sample.txt") == 1320
  end

  test "part 2" do
    # assert LensLibrary2.run("inputs/15/sample.txt") == 0
  end
end
