defmodule PointOfIncidenceTest do
  use ExUnit.Case

  test "part 1" do
    assert PointOfIncidence.run("inputs/13/sample.txt") == 405
  end

  test "part 2" do
    assert PointOfIncidence2.run("inputs/13/sample.txt") == 400
  end
end
