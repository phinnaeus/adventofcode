defmodule HotSpringsTest do
  use ExUnit.Case

  test "part 1" do
    assert HotSprings.run("inputs/12/sample.txt") == 21
  end

  test "part 2" do
    # assert HotSprings2.run("inputs/12/sample.txt") == 0
  end
end
