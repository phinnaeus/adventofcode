defmodule TrebuchetTest do
  use ExUnit.Case

  test "part 1" do
    assert Trebuchet.run("inputs/01/sample.txt") == 142
  end

  test "part 2" do
    assert Trebuchet2.run("inputs/01/sample2.txt") == 281
  end
end
