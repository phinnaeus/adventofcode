defmodule ReflectorTest do
  use ExUnit.Case

  test "part 1" do
    assert Reflector.run("inputs/14/sample.txt") == 136
  end

  test "part 2" do
    assert Reflector2.run("inputs/14/sample.txt") == 64
  end
end
