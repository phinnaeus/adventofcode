defmodule BoatRacesTest do
  use ExUnit.Case

  test "part 1" do
    assert BoatRaces.ways_to_win_all("inputs/06/sample.txt") == 288
  end

  test "part 2" do
    assert BoatRaces2.ways_to_win_all("inputs/06/sample.txt") == 71_503
  end
end
