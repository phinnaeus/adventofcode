defmodule DesertMapTest do
  use ExUnit.Case

  test "part 1" do
    assert DesertMap.how_many_steps("inputs/08/sample.txt") == 2
    assert DesertMap.how_many_steps("inputs/08/sample2.txt") == 6
  end

  test "part 2" do
    assert DesertMap2.how_many_steps("inputs/08/sample3.txt") == 6
  end
end
