# Advent of Code 2023

I'm learning Elixir, so please keep that in mind if you're going to judge this code.

## Structure
`lib`: Implementation of the solution for each day. Some folders contain multiple files, this usually corresponds to different parts of each puzzle.
`inputs`: Inputs, both samples and "real" inputs.
`test`: Test cases for all the sample inputs. Run them with `mix test`.

## Routine

1. `mix new_day <NameOfPuzzle>`: This will scaffold the files needed for each new puzzle.
2. Copy/paste the sample from the puzzle description and fill in the correct value in the generated test.
3. Download the input file and save it next to sample.txt as `input.txt`
4. `iex -S mix` to open a REPL where you can simply run `NameOfPuzzle.run("<path to sample.txt>")` for testing or `NameOfPuzzle.run()` to try and get the final result. Alternatively `mix test`.

## Tools
`mix format`: format the code
`mix credo`: some linter thing I found